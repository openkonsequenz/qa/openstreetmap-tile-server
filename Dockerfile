FROM overv/openstreetmap-tile-server:1.4.0

# Start running
COPY run.sh /
RUN chown renderer /run.sh
RUN chmod +x /run.sh

COPY render_list_geo.pl /
RUN chmod +x /render_list_geo.pl

EXPOSE 80 5432